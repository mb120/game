import 'dart:io';
import 'game.dart';

class WordQuiz extends Game {
  late int quiz;
  late String answer;
  late int returnQuiz = 1;
  late int hint = 0;
  late int count = 0;

  WordQuiz() {}

  // int getCoin() {
  //   return this.coin;
  // }

  // void setCoin(int coin) {
  //   this.coin = coin;
  // }

  void showWelcome() {
    print("----------------------------------------------------");
    print("WELCOME TO GAME WORDQUIZ");
    print("----------------------------------------------------");
  }

  void showRule() {
    print("Rule Of WordQuiz Game:");
    print("-Answers in English capital letters or numbers only.-");
    print("----------------------------------------------------\n");
  }

  int showQuiz(quiz, int coin) {
    switch (quiz) {
      case 1:
        print("1. How many hours are there in 1 day? ");
        useHint(1, coin);
        stdout.write("\nInput your answer:  ");
        answer = stdin.readLineSync()!;
        print("\n");
        if (answer == "24") {
          if (hint == 1) {
            coin = (coin + 100) - 50;
            // setCoin(coin);
            //print("coin : $coin");
            count++;
            return coin;
          } else {
            coin = coin + 100;
            // setCoin(coin);
            //print("coin : $coin");
            count++;
            return coin;
          }
        } else {
          if (hint == 1) {
            coin = coin - 100;
            count++;
            return coin;
            // setCoin(coin);
          } else {
            coin = coin - 50;
            count++;
            return coin;
            // setCoin(coin);
          }
        }
      case 2:
        print("\n");
        print("----------------------------------------------------\n");
        print("2. How many leap years in February will have 29 days? ");
        useHint(2, coin);
        stdout.write("\nInput your answer: ");
        answer = stdin.readLineSync()!;
        print("\n");
        print("NEXT TO MINIGAME FINDWORD NUMBER 2");
        print("----------------------------------------------------\n");
        if (answer == "4") {
          if (hint == 1) {
            coin = (coin + 100) - 50;
            // setCoin(coin);
            //print("coin : $coin");
            count++;
            return coin;
          } else {
            coin = coin + 100;
            // setCoin(coin);
            //print("coin : $coin");
            count++;
            return coin;
          }
        } else {
          if (hint == 1) {
            coin = coin - 100;
            // setCoin(coin);
            count++;
            return coin;
          } else {
            coin = coin - 50;
            // setCoin(coin);
            count++;
            return coin;
          }
        }
      case 3:
        print("\n");
        print("----------------------------------------------------\n");
        print("3. Which way the sun rises(N E W or S)? ");
        useHint(3, coin);
        stdout.write("\nInput your answer: ");
        answer = stdin.readLineSync()!;
        print("\n");
        print("NEXT TO MINIGAME FINDWORD NUMBER 3");
        print("----------------------------------------------------\n");
        if (answer == "E") {
          if (hint == 1) {
            coin = (coin + 100) - 50;
            // setCoin(coin);
            //print("coin : $coin");
            count++;
            return coin;
          } else {
            coin = coin + 100;
            // setCoin(coin);
            //print("coin : $coin");
            count++;
            return coin;
          }
        } else {
          if (hint == 1) {
            coin = coin - 100;
            // setCoin(coin);
            count++;
            return coin;
          } else {
            coin = coin - 50;
            // setCoin(coin);
            count++;
            return coin;
          }
        }
      case 4:
        print("\n");
        print("----------------------------------------------------\n");
        print("4. What is the world's largest country? ");
        useHint(4, coin);
        stdout.write("\nInput your answer: ");
        answer = stdin.readLineSync()!;
        print("\n");
        print("NEXT TO MINIGAME FINDWORD NUMBER 4");
        print("----------------------------------------------------\n");
        if (answer == "RUSSIA") {
          if (hint == 1) {
            coin = (coin + 100) - 50;
            // setCoin(coin);
            //print("coin : $coin");
            count++;
            return coin;
          } else {
            coin = coin + 100;
            // setCoin(coin);
            //print("coin : $coin");
            count++;
            return coin;
          }
        } else {
          if (hint == 1) {
            coin = coin - 100;
            // setCoin(coin);
            count++;
            return coin;
          } else {
            coin = coin - 50;
            // setCoin(coin);
            count++;
            return coin;
          }
        }
      case 5:
        print("\n");
        print("----------------------------------------------------\n");
        print("5.H2O how many atoms ");
        useHint(5, coin);
        stdout.write("\nInput your answer: ");
        answer = stdin.readLineSync()!;
        print("\n");
        print("NEXT TO MINIGAME FINDWORD NUMBER 5");
        print("----------------------------------------------------\n");
        if (answer == "18") {
          if (hint == 1) {
            coin = (coin + 100) - 50;
            // setCoin(coin);
            //print("coin : $coin");
            count++;
            return coin;
          } else {
            coin = coin + 100;
            // setCoin(coin);
            //print("coin : $coin");
            count++;
            return coin;
          }
        } else {
          if (hint == 1) {
            coin = coin - 100;
            // setCoin(coin);
            count++;
            return coin;
          } else {
            coin = coin - 50;
            // setCoin(coin);
            count++;
            return coin;
          }
        }
    }
    return coin;
  }

  bool useHint(int quiz, int coin) {
    print("> Hint : use or not use");
    print("type 1 for choose use :");
    print("type 2 for choose not use :");
    stdout.write("\nInput your number: ");
    String num = stdin.readLineSync()!;
    if (num == "1" && coin > 0) {
      switch (quiz) {
        case 1:
          hint = 1;
          print("Hint: ");
          print("Half day has 12 hours.");
          break;
        case 2:
          hint = 1;
          print("Hint: ");
          print("Two + Two");
          break;
        case 3:
          hint = 1;
          print("Hint: ");
          print("opposite west.");
          break;
        case 4:
          hint = 1;
          print("Hint: ");
          print("Back Forward: A I S S U R");
          break;
        case 5:
          hint = 1;
          print("Hint: ");
          print("H = 1 , O = 16");
          break;
      }
      return true;
    } else if (num == "2") {
      return false;
    } else if (num == "1" && coin <= 0) {
      hint = 1;
      print("Not available, not enough money");
      return false;
    } else {
      print("Please input your number!!!");
      useHint(quiz, coin);
    }
    return false;
  }

  void showResult(int coin) {
    print("coin = $coin");
    print("You answered $count questions correctly.!!");
    if (count <= 1) {
      print("Let's go back and start all over again.");
    } else if (count == 2) {
      print("you try well.");
    } else if (count == 3) {
      print("you are getting better.");
    } else if (count == 4) {
      print("you are very clever.");
    } else {
      print("You are the superhuman in answering questions.");
    }
  }
}
