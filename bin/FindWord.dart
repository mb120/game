import 'Game.dart';

class FindWord extends Game{
  late int x, y;
  late String symbol = "#";

  void showWelcome() {
    print("----------------------------------------------------");
    print("WELCOME TO GAME Find Word in map");
    print("----------------------------------------------------");
  }

  void showRule() {
    print("Rule Of Find Word in map Game:");
    print("-Must walk to collect all the letters in the map.-");
    print("----------------------------------------------------\n");
  }

  FindWord(int x, int y){
    this.x = x;
    this.y = y;
    this.symbol = "#";
  }

  int getX(){
    return x;
  }

  int getY(){
    return y;
  }

  String getSymbol(){
    return symbol;
  }

  bool isOn(int x, int y){
    return this.x == x && this.y == y;
  }
}