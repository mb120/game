class Obj{
  late int x,y;
  late String symbol;

  Obj(String symbol, int x, int y){
    this.x = x;
    this.y = y;
    this.symbol = symbol;
  }

  int getX(){
    return x;
  }

  int getY(){
    return y;
  }

  String getSymbol(){
    return symbol;
  }

  bool isOn(int x, int y){
    return this.x == x && this.y == y;
  }

    String fill(){
    symbol = "-";
    return symbol;
  }
}