import 'FindWord.dart';
import 'Game.dart';
import 'WordQuiz.dart';

void main(List<String> arguments) {
  WordQuiz wordQuiz = new WordQuiz();
  Game game = new Game();
  FindWord findWord = new FindWord(0, 0);
  late int coin = 100;

  wordQuiz.showWelcome();
  wordQuiz.showRule();

  //Run quiz
  for (int i = 1; i <= 5; i++) {
    coin = wordQuiz.showQuiz(i, coin);
    if (i == 1) {
      findWord.showWelcome();
      findWord.showRule();
    }
    coin = game.switchMapQuiz(i, coin);
  }

  wordQuiz.showResult(coin);
}
