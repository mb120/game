import 'dart:io';
import 'PlayerMap.dart';
import 'TableMap.dart';
import 'Word.dart';

class Game {
  late int level;

  Game() {

  }

  void showWelcome() {
                                                                                                          
  }

  void showRule() {
  
  }

  int switchMapQuiz(int level, int coin) {
    switch (level) {
      case 1:
        coin = MapQuiz1(coin);
        return coin;
      case 2:
        coin = MapQuiz2(coin);
        return coin;
      case 3:
        coin = MapQuiz3(coin);
        return coin;
      case 4:
        coin = MapQuiz4(coin);
        return coin;
      case 5:
        coin = MapQuiz5(coin);
        return coin;
    }
    return coin;
  }

  int MapQuiz1(int coin){
    TableMap map = new TableMap(10, 5);
    int count = 0;
    PlayerMap playerMap = new PlayerMap(1, 1, "#", map);
    Word quiz1_1 = new Word('G',2,3);
    Word quiz1_2 = new Word('A',4,2);
    Word quiz1_3 = new Word('M',5,3); 
    Word quiz1_4 = new Word('E',8,4);

    map.setPlayerMap(playerMap); 
    map.setWord(quiz1_1);
    map.setWord(quiz1_2);
    map.setWord(quiz1_3);
    map.setWord(quiz1_4);

    while(true){
      map.showMap();
      count;
      // W=a, N=w, E=d, S=s, q=quit 
      print("> Select direction: ");
      print("Input a = go West ");
      print("Input w = go North ");
      print("Input s = go South ");
      print("Input d = go East ");
      print("Input q = Exit Game ");
      stdout.write("\nPlease input your direction: ");
      String direction = stdin.readLineSync()!;
      if(direction == "q") {
        print("GoodBye Thanks for play :O) ") ;
        break;
        }
        playerMap.walk(direction);
        count = playerMap.checkFindWord1(count);
      if(count == 4){
        coin += 50;
        map.showMap();
        print("mix words: GAME");
        print("You received coin 50\n");
        print("NEXT TO WORDQUIZ NUMBER 2 ");
        return coin;
      }
    }
    return coin;
  }

  int MapQuiz2(int coin){
    TableMap map = new TableMap(10, 5);
    int count = 0;
    PlayerMap playerMap = new PlayerMap(1, 1, "#", map);
    Word quiz2_1 = new Word('B',2,2);
    Word quiz2_2 = new Word('U',4,3);
    Word quiz2_3 = new Word('R',7,3); 
    Word quiz2_4 = new Word('A',9,1);
    Word quiz2_5 = new Word('P',8,2);
    Word quiz2_6 = new Word('H',6,4);
    Word quiz2_7 = new Word('A',5,2);


    map.setPlayerMap(playerMap); 
    map.setWord(quiz2_1);
    map.setWord(quiz2_2);
    map.setWord(quiz2_3);
    map.setWord(quiz2_4);
    map.setWord(quiz2_5);
    map.setWord(quiz2_6);
    map.setWord(quiz2_7);

    while(true){
      map.showMap();
      count;
      // W=a, N=w, E=d, S=s, q=quit
      print("> Select direction: ");
      print("Input a = go West ");
      print("Input w = go North ");
      print("Input s = go South ");
      print("Input d = go East ");
      print("Input q = Exit Game ");
      stdout.write("\nPlease input your direction: ");
      String direction = stdin.readLineSync()!;
      if(direction == "q"){
        print("GoodBye  Thanks for play :O) ") ;
        break;
        }
        playerMap.walk(direction);
        count = playerMap.checkFindWord2(count);
      if(count == 7){
        coin += 50;
        map.showMap();
        print("mix words: BURAPHA");
        print("You received coin 50\n");
        print("NEXT TO WORDQUIZ NUMBER 3 ");
        return coin;
      }
    }
    return coin;

  }
  
  int MapQuiz3(int coin){
    TableMap map = new TableMap(10, 5);
    int count = 0;
    PlayerMap playerMap = new PlayerMap(1, 1, "#", map);
    Word quiz3_1 = new Word('S',8,4);
    Word quiz3_2 = new Word('C',5,4);
    Word quiz3_3 = new Word('I',4,2); 
    Word quiz3_4 = new Word('E',3,1);
    Word quiz3_5 = new Word('N',4,1);
    Word quiz3_6 = new Word('C',6,2);
    Word quiz3_7 = new Word('E',9,3);


    map.setPlayerMap(playerMap); 
    map.setWord(quiz3_1);
    map.setWord(quiz3_2);
    map.setWord(quiz3_3);
    map.setWord(quiz3_4);
    map.setWord(quiz3_5);
    map.setWord(quiz3_6);
    map.setWord(quiz3_7);

    while(true){
      map.showMap();
      count;
      // W=a, N=w, E=d, S=s, q=quit 
      print("> Select direction: ");
      print("Input a = go West ");
      print("Input w = go North ");
      print("Input s = go South ");
      print("Input d = go East ");
      print("Input q = Exit Game ");
      stdout.write("\nPlease input your direction: ");
      String direction = stdin.readLineSync()!;
      if(direction == "q"){
        print("GoodBye  Thanks for play :O) ") ;
        break;
        }
        playerMap.walk(direction);
         count = playerMap.checkFindWord3(count);
      if(count == 7){
        coin += 50;
        map.showMap();
        print("mix words: SCIENCE");
        print("You received coin 50\n");
        print("NEXT TO WORDQUIZ NUMBER 4 ");
        return coin;
      }
    }
    return coin;

  }

  int MapQuiz4(int coin){
    TableMap map = new TableMap(10, 5);
    int count = 0;
    PlayerMap playerMap = new PlayerMap(1, 1, "#", map);
    Word quiz4_1 = new Word('C',5,1);
    Word quiz4_2 = new Word('O',8,0);
    Word quiz4_3 = new Word('M',3,2); 
    Word quiz4_4 = new Word('P',3,1);
    Word quiz4_5 = new Word('U',6,4);
    Word quiz4_6 = new Word('T',5,2);
    Word quiz4_7 = new Word('E',0,3);
    Word quiz4_8 = new Word('R',9,3);

    map.setPlayerMap(playerMap); 
    map.setWord(quiz4_1);
    map.setWord(quiz4_2);
    map.setWord(quiz4_3);
    map.setWord(quiz4_4);
    map.setWord(quiz4_5);
    map.setWord(quiz4_6);
    map.setWord(quiz4_7);
    map.setWord(quiz4_8);

    while(true){
      map.showMap();
      count;
      // W=a, N=w, E=d, S=s, q=quit 
      print("> Select direction: ");
      print("Input a = go West ");
      print("Input w = go North ");
      print("Input s = go South ");
      print("Input d = go East ");
      print("Input q = Exit Game ");
      stdout.write("\nPlease input your direction: ");
      String direction = stdin.readLineSync()!;
      if(direction == "q"){
        print("GoodBye Thanks for play :O) ") ;
        break;
        }
        playerMap.walk(direction);
        count = playerMap.checkFindWord4(count);
      if(count == 8){
        coin += 50;
        map.showMap();
        print("mix words: COMPUTER");
        print("You received coin 50\n");
        print("NEXT TO WORDQUIZ NUMBER 5 ");
        return coin;
      }
    }
    return coin;

  }

  int MapQuiz5(int coin){
    TableMap map = new TableMap(10, 5);
    int count = 0;
    PlayerMap playerMap = new PlayerMap(1, 1, "#", map);
    Word quiz5_1 = new Word('F',6,0);
    Word quiz5_2 = new Word('I',0,4);
    Word quiz5_3 = new Word('N',9,0); 
    Word quiz5_4 = new Word('D',5,1);
    Word quiz5_5 = new Word('W',2,4);
    Word quiz5_6 = new Word('O',9,1);
    Word quiz5_7 = new Word('R',8,3);
    Word quiz5_8 = new Word('D',2,2);

    map.setPlayerMap(playerMap); 
    map.setWord(quiz5_1);
    map.setWord(quiz5_2);
    map.setWord(quiz5_3);
    map.setWord(quiz5_4);
    map.setWord(quiz5_5);
    map.setWord(quiz5_6);
    map.setWord(quiz5_7);
    map.setWord(quiz5_8);

    while(true){
      map.showMap();
      count;
      // W=a, N=w, E=d, S=s, q=quit 
      print("> Select direction: ");
      print("Input a = go West ");
      print("Input w = go North ");
      print("Input s = go South ");
      print("Input d = go East ");
      print("Input q = Exit Game ");
      stdout.write("\nPlease input your direction: ");
      String direction = stdin.readLineSync()!;
      if(direction == "q"){
        print("GoodBye  Thanks for play :O) ") ;
        break;
        }
        playerMap.walk(direction);
        count = playerMap.checkFindWord5(count);
      if(count == 8){
        coin += 50;
        map.showMap();
        print("mix words: FINDWORD");
        print("You received coin 50\n");
        print("Let's go the answers to see how many answers are correct!!!!!\n");
        return coin;
      }
    }
    return coin;

  }


}