import 'dart:io';
import 'FindWord.dart';
import 'Obj.dart';
import 'PlayerMap.dart';
import 'Word.dart';

class TableMap{
  late int width=10;
  late int height=5;
  FindWord findword= FindWord(0, 0);
  late PlayerMap playerMap ;
  int objCount = 0;
  late Word word;
  List<Obj> objs = [
    Obj("", 0, 0), Obj("", 0, 0),
    Obj("", 0, 0), Obj("", 0, 0),
    Obj("", 0, 0), Obj("", 0, 0),
    Obj("", 0, 0), Obj("", 0, 0),
    Obj("", 0, 0), Obj("", 0, 0),
    Obj("", 0, 0), Obj("", 0, 0),
    Obj("", 0, 0), Obj("", 0, 0),
    Obj("", 0, 0), Obj("", 0, 0),
    Obj("", 0, 0), Obj("", 0, 0),
    Obj("", 0, 0), Obj("", 0, 0),
    Obj("", 0, 0), Obj("", 0, 0),
    Obj("", 0, 0), Obj("", 0, 0),
    Obj("", 0, 0), Obj("", 0, 0),
    Obj("", 0, 0), Obj("", 0, 0),
    Obj("", 0, 0), Obj("", 0, 0),
    Obj("", 0, 0), Obj("", 0, 0),
    Obj("", 0, 0), Obj("", 0, 0),
    Obj("", 0, 0)
    ];
  

  TableMap(int width, int height){
    this.width = width;
    this.height = height;
  }

  void addObj(Obj obj){
    objs[objCount] = obj;
    objCount++;
    //print(obj.getSymbol()+" Added");

  }

  void setPlayerMap(PlayerMap playerMap) {
        this.playerMap = playerMap;
        addObj(playerMap);
  }

  void setWord(Word word) {
        this.word = word;   
        addObj(word);
  }


  void setfindword(FindWord findword){
    this.findword = findword;
  }

  void showMap() {
       print("+++ Map +++");
       for(int y = 0; y < height; y++){
        for(int x = 0;x < width; x++){
          printSymbolOn(x,y);
       }
       print("\n");
      } 
  }

  void printSymbolOn(int x, int y){
    String symbol ="-";
    for (int i=0; i < objCount; i++) {
      if(objs[i].isOn(x,y)){
        symbol = objs[i].getSymbol();
      }
    }
    stdout.write(symbol);
  }

  void showfindword(){
    print(findword.getSymbol());
  }

  void showPlayerMap(){
    print(playerMap.getSymbol());
  }

  bool inmap(int x, int y){
    //  x -> 0-(width-1),y -> 0-(height-1)
        return (x >= 0 && x < width) && (y >= 0 && y < height);
  }

  bool isFindword(int x,int y){
    return findword.isOn(x, y);
  }

  String reSym(int x, int y){
    for(int i = 0; i< objCount; i++){
      if(objs[i] is Word && objs[i].isOn(x, y)){
        Obj fw = objs[i];
        return fw.fill();
      }
    }
    return "";
  }


}