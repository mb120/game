import 'Obj.dart';
import 'TableMap.dart';
import 'WordQuiz.dart';

class PlayerMap extends Obj{
  late int x, y;
  late String symbol;
  TableMap map = TableMap(0, 0);
  late int count = 0;
  late int coin = 0;
  WordQuiz wordQuiz = WordQuiz();

  PlayerMap(int x,int y, String symbol, TableMap map):super(symbol, x, y){
    this.x = x;
    this.y = y;
    this.symbol = symbol;
    this.map = map;
  }

  int getX(){ 
    return x;
  }

  int getY(){
    return y;
  }

  String getSymbol(){
    return symbol;
  }

  bool walk(String direction){
    switch (direction) {
          case 'N': 
            case 'w':
                if (walkN()) return false;
                break;
          case 'S':
            case 's':
                if (walkS()) return false;
                break;
          case 'E':
            case 'd':
                if (walkE()) return false;
                break;
          case 'W':
            case 'a':
                if (walkW()) return false;
                break;
            default:
                return false;
        }

      return false;
  }

  bool walkW(){
    if (canWalk(x - 1, y)) {
            x = x - 1;
        } else {
            return true;
        }
    return false;
  }

  bool walkE(){
    if (canWalk(x + 1, y)) {
            x = x + 1;
        } else {
            return true;
        }
    return false;
  }

  bool walkS(){
    if (canWalk(x, y + 1)) {
            y = y + 1;
        } else {
            return true;
        }
    return false;
  }

  bool walkN(){
    if (canWalk(x, y - 1)) {
            y = y - 1;
        } else {
            return true;
        }
    return false;
  }


  int checkFindWord1(int count){
    if(map.isFindword(x, y) ||
    (x == 2 && y == 3) ||
    (x == 4 && y == 2) ||
    (x == 5 && y == 3) ||
    (x == 8 && y == 4) ){
    // print("Founded word!!!! $x , $y ");
    map.reSym(x, y) ;
    count++;
      if(count == 4){
        return 4;
      }
    }
    return count;
  }

  int checkFindWord2(int count){
    if(map.isFindword(x, y) ||
    (x == 2 && y == 2) ||
    (x == 4 && y == 3) ||
    (x == 7 && y == 3) ||
    (x == 9 && y == 1) ||
    (x == 8 && y == 2) ||
    (x == 6 && y == 4) ||
    (x == 5 && y == 2) ){
    // print("Founded word!!!! $x , $y ");
    map.reSym(x, y) ;
    count++;
      if(count == 7){
        return 7;
      }
    }
    return count;
  
  }

  int checkFindWord3(int count){
    if(map.isFindword(x, y) ||
    (x == 8 && y == 4) ||
    (x == 5 && y == 4) ||
    (x == 4 && y == 2) ||
    (x == 3 && y == 1) ||
    (x == 4 && y == 1) ||
    (x == 6 && y == 2) ||
    (x == 9 && y == 3) ){
    // print("Founded word!!!! $x , $y ");
    map.reSym(x, y) ;
    count++;
      if(count == 7){
        return 7;
      }
    }
    return count;
  }

  int checkFindWord4(int count){
    if(map.isFindword(x, y) ||
    (x == 5 && y == 1) ||
    (x == 8 && y == 0) ||
    (x == 3 && y == 2) ||
    (x == 3 && y == 1) ||
    (x == 6 && y == 4) ||
    (x == 5 && y == 2) ||
    (x == 0 && y == 3) ||
    (x == 9 && y == 3) ){
    //print("Founded word!!!! $x , $y ");
    map.reSym(x, y) ;
    count++;
      if(count == 8){
        return 8;
      }
    }
    return count;
  }

  int checkFindWord5(int count){
    if(map.isFindword(x, y) ||
    (x == 6 && y == 0) ||
    (x == 0 && y == 4) ||
    (x == 9 && y == 0) ||
    (x == 5 && y == 1) ||
    (x == 2 && y == 4) ||
    (x == 9 && y == 1) ||
    (x == 8 && y == 3) ||
    (x == 2 && y == 2) ){
    //print("Founded word!!!! $x , $y ");
    map.reSym(x, y) ;
    count++;
      if(count == 8){
        return 8;
      }
    }
    return count;
  }

  bool isOn(int x, int y){
    return this.x == x && this.y == y;
  }

  bool canWalk(int x, int y){
    return map.inmap(x, y);
  }

 


}